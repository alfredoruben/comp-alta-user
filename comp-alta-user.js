{
  const {
    html,
  } = Polymer;
  /**
    `<comp-alta-user>` Description.

    Example:

    ```html
    <comp-alta-user></comp-alta-user>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --comp-alta-user | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CompAltaUser extends Polymer.Element {

    static get is() {
      return 'comp-alta-user';
    }

    static get properties() {
      return {      
        "_idCliente": {
        type: Number,
        value:""
      },
      "_nombres": {
        type: String,
        value:""
      },
      "_appaterno": {
        type: String,
        value:""
      },
      "_dni": {
        type: String,
        value:""
      },
      "_pasword": {
        type: String,
        value:""
      }
    };
    }

    
  _addConctact() {
    console.info('cells-crud-view::_addConctact... begin');
    
    var contact = {
      "firstName": this._nombres,
      "lastName": this._appaterno,
      "dni": this._dni,
      "clave": this._pasword
    }
    
/*
    var contact = {
      "idCliente": "10",
      "Nombre": "Bartolito",
      "ApePaterno": "Carrera",
      "ApeMaterno": "Carrera"
    }
 */

    if(contact.firstName == ''){
      return false;
    }
    if(contact.lastName == ''){
      return false;
    }
    if(contact.dni == ''){
      return false;
    }
    if(contact.clave == ''){
      return false;
    }
    this.dispatchEvent(new CustomEvent('add-contact', {composed: true, bubbles: true, detail: contact}));
   //this.$.addContactModal.set('open', false);
    console.info('com-alta-user::_addConctact... end');
  }

    static get template() {
      return html `
      <style include="comp-alta-user-styles comp-alta-user-shared-styles"></style>
      <slot></slot>
      
          <p>Alta Usuarios</p>
          <p>
  <cells-molecule-input name="txtNombre" label="Nombres" type="text" value={{_nombres}}>
  </cells-molecule-input>
</p>
<p>
<cells-molecule-input name="txtApePaterno" label="Apellidos" type="text" value={{_appaterno}}>
</cells-molecule-input>
</p>
<p>
<cells-molecule-input name="txtDni" label="Dni" type="text" value={{_dni}}>
</cells-molecule-input>
</p>
<p>
<cells-molecule-input label="Password" type="password" icon="coronita:visualize" icon-toggled="coronita:hide" icon-label="View or hide password" extra-icon="coronita:close" extra-icon-label="Clear field" value={{_pasword}}>
</cells-molecule-input>
</p>  
<cells-st-button class="primary">
  <button on-click="_addConctact">Guardar</button>
</cells-st-button>   
      `;
    }
  }

  
  customElements.define(CompAltaUser.is, CompAltaUser);
}